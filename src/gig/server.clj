(ns gig.server
  (:require
    [immutant.web :as web]
    [compojure.core :as cj]
    [compojure.route :as cjr]
    [rum.core :as rum])
  (:import
    [org.joda.time.format DateTimeFormat]
    (org.joda.time DateTime)))

; "/"
; "/feed"
; "/post/:id"
; "/login"
; GET "/write"
; POST "/write"

(def posts
  [{:id      123
    :created #inst "2018-07-29"
    :author  "გიგა ჩოხელი"
    :body    "პროგრამისტი პირველ რიგში გეტყვით რომ არაა მეზობელი რომელმაც “გადაგიყენათ” შუშაბანდი ანუ Windows, ან თუ თქვენ ბრძანდებით ზემოთხსენებული ადამიანი იმედი უნდა გაგიცრუოთ, მაგრამ სულ სხვა საქმით ხართ დაკავებული. ნურც ფილმებში დახატული გენიოსები შეგაშინებთ - თვალდახუჭულნი 1-სა და 0-ების გაბმულ სვეტებს რომ წერენ - ერთ საიდუმლოს გაგიმხელთ ამ ფილმების არცერთ სცენარის ავტორსა თუ რეჟისორს წარმოდგენა არ აქვს პროგრამირებაზე."}
   {:id      124
    :created #inst "2018-07-28"
    :author  "გიგა ჩოხელი"
    :body    "პროგრამების მაგალითი ქართულ ენაზე: გააკეთე ეს, შემდეგ ეს ორ ეტაპად: თუ ეს პირობა ჭეშმარიტია - შეასრულე ეს ოპერაცია ყველა სხვა შემთხვევაში - შეასრულე შემდეგი შეასრულე შემდეგი ოპერაცია 2015-ჯერ შეუჩერებლად აკეთე ესა და ეს, სანამ პირობა ჭეშმარიტია "}])

(def date-formatter (DateTimeFormat/forPattern "dd.MM.YYYY"))

(defn render-date [inst]
  (.print date-formatter (DateTime. inst)))

(rum/defc post [post]
  [:.post
   [:.post_sidebar
    [:.img.avatar {:src (str "/i/" (:author post) "*.jpg")}]]
   [:div
    [:p [:span.author (:author post) ": "] (:body post)]
    [:p.meta (render-date(:created post)) " // " [:a {:href (str "/post/" (:id post))} "ლინკი"]]]])

(declare styles)

(rum/defc page [title & children]
  [:html
   [:head
    [:title title]]
   [:meta {:charset "utf-8"}]
   [:meta {:name "viewport" :content "width=device-width, initial-scale=1.0"}]
   [:style styles]
   [:body
    [:header
     [:h1 "21-ე საუკუნის ანბანი"
      [:p#site_subtitle "რა არის პროგრამირება?"]]
     children]]
   [:footer
    [:a {:href "http://chokhe.li"} "გიგა ჩოხელი"]
    ". 2018. ყველა უფლება დაცულია."
    [:br]
    [:a {:href "/feed" :rel "alternate" :type "application/rss+xml"} "RSS"]]])

(rum/defc index [posts]
  (page "21-ე საუკუნის ანბანი"
        (for [p posts]
          (post p))))

(defn render-html [component]
  (str "<DOCTYPE html>\n"
       (rum/render-static-markup component)))

(cj/defroutes routes
              (cj/GET "/" [:as req]
                {:body (render-html (index posts))})

              (cj/GET "/write" [:as req]
                {:body "WRITE"})

              (cj/POST "/write" [:as req]
                {:body "POST"}))

(defn with-headers [handler headers]
  (fn [request]
    (some-> (handler request)
            (update :headers merge headers))))

(def app
  (-> routes
      (with-headers {"Content-Type"  "text/html; charset=utf-8"
                     "Cache-Control" "no-cache"
                     "Expires"       "-1"})))

(defn -main [& args]
  (let [args-map (apply array-map args)
        port-str (or (get args-map "-p")
                     (get args-map "--port")
                     ("8080"))]
    (web/run #'app {:port (Integer/parseInt port-str)})))

(comment
  (def server (-main "--port" "8080"))
  (web/stop server))

(def styles
  "* { box-sizing: border-box; }

  body {
      font-family: sans-serif;
      line-height: 140%;
      padding: 0.25em 1em;
      max-width: 652px; /* .post_img =>> 550px */
      margin: 0 auto;
      transition: 500ms ease-in-out;
  }
  
  .icon_rotate { width: 20px; height: 20px; display: inline-block; background: url(\"/static/rotate.svg\"); margin: 5px 5px -5px -25px; }
  @media (max-width: 679px) {
      .icon_rotate { display: none; }
  }
  
  /* HEADER */
  
  header { margin-bottom: 2em; }
  
  .title { margin-bottom: 0; }
  .title_back { position: absolute; margin-left: -1em; text-decoration: none; }
  .title_back:hover { text-decoration: underline; }
  
  .title_new { padding: 10px; margin: -10px 0 0 0; float: right; text-decoration: none; color: #c3c; }
  .title_new:hover { color: #d4d; }
  body.anonymous .title_new { display: none; }
  
  .subtitle { margin-left: 0.09em;
      -webkit-touch-callout: none;
      -webkit-user-select: none;
      -khtml-user-select: none;
      -moz-user-select: none;
      -ms-user-select: none;
      user-select: none; }
  .subtitle > span { cursor: pointer; }
  
  
  /* POST */
  
  .post { display: flex; justify-content: flex-start; margin-bottom: 2em; }
  .post_side { margin-right: 20px; min-width: 50px; }
  @media (max-width: 399px) {
      .post_side { display: none; }
  }
  .post_avatar { width: 50px; height: 50px; }
  
  .post_video_outer { position: relative; }
  .post_video_overlay { position: absolute; top: 0; bottom: 0; left: 0; right: 0; cursor: pointer; }
  .post_video_overlay-paused { background-color: rgba(0,0,0,0.3); background-image: url(\"/static/play.svg\"); background-size: contain; background-position: center center; background-repeat: no-repeat; }
  .post_video_overlay-paused:hover { background-color: rgba(0,0,0,0.25); }
  .post_img, .post_video { display: block; margin-bottom: 1em; }
  .post_img-fix { display: block; background-size: 100%; position: relative; }
  .post_img-fix > img { position: absolute; height: 100%; }
  .post_video,
  .post_img-flex > img { display: block; max-width: 100%; height: auto; max-height: 500px; }
  .post_author { font-weight: bold; }
  .post_content { width: 100%; }
  .post_body > p { margin: 0 0 1em 0; }
  
  .post_meta_edit { float: right; color: #c3c; }
  .post_meta_edit:hover { color: #d4d; }
  body.anonymous .post_meta_edit { display: none; }
  
  
  /* FOOTER */
  @keyframes rotating {
      from { transform: rotate(0deg); }
      to { transform: rotate(360deg); }
  }
  
  .loader { margin: 0 0 3em 0; text-align: center; }
  .loader > img { width: 76px; height: 76px; border: 2px solid #000; border-radius: 50%; }
  .loader-loading > img { animation: rotating 1s ease-in-out infinite; }
  .loader-error > img { cursor: pointer; border-color: red; }
  .loader-error > img:hover { position: relative; top: -1px; }
  
  footer {
      padding: 1em 0 1em;
      margin-top: 2em;
      border-top: 1px solid #000;
  }")
# Gig

Yet another blog _engine_ written in Clojure.

## Usage

Well, it depends, in this very case, the idea is to liberate myself from constrains of other blog engines and build up functionality that I do actually need.

## Tech Stack
- Clojure 1.9
- Ring (core) http handler
- immutant web server
- compojure routing library
- rum ClojureScript wrapper for React (you should check this one out)

## License

Copyright © 2018 Giga Chokheli

Distributed under the Eclipse Public License version 1.0
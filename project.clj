(defproject gig "0.1.0-SNAPSHOT"
  :dependencies [[org.clojure/clojure   "1.9.0"]
                 [ring/ring-core        "1.6.3" :exclusions [[ring/ring-codec]
                                                             [crypto-random]
                                                             [commons-codec]]]
                 [org.immutant/web      "2.1.10"]
                 [compojure             "1.6.1"]
                 [rum                   "0.11.2"]])